import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { filter, isNumber } from 'lodash';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Todo } from '../models/todo';

@Injectable({ providedIn: 'root' })
export class TodoService {

  private _todos: Todo[];

  constructor() {
    this._todos = JSON.parse(localStorage.getItem('todos'));

    if (this._todos == null) {
      this._todos = [
        new Todo(1, 'Nettoyer le bureau', false, new Date()),
        new Todo(2, 'Aller faire les course', true, new Date()),
        new Todo(3, 'Récupérer les enfants à l\'école', false, new Date()),
      ];
    }
  }

  private saveInLocalStorage(): void {
    localStorage.setItem('todos', JSON.stringify(this._todos));
  }

  public getTodo(id: number): Observable<Todo> {
    return of(this._todos.find(t => t.id == id));
  }

  public createOrUpdateTodo(todo: Todo): Observable<Todo> {
    // If null, we are in creation
    if (todo.id == null) {

      todo.id = _.max(this._todos.map(t => t.id)) + 1;
      this._todos.push(todo);
    }

    this.saveInLocalStorage();

    return this.getTodo(todo.id);
  }

  public getTodos(): Observable<Todo[]> {
    const obs$: Observable<Todo[]> = of(this._todos);

    console.log('TodoService - getTodos()');
    return obs$;
  }

  public deleteTodo(id: number): Observable<void> {

    _.remove(this._todos, t => t.id === id);

    this.saveInLocalStorage();

    return of();
  }
}
