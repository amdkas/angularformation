import { Component, OnDestroy, OnInit } from '@angular/core';
import { map } from 'lodash';
import { Observable, Subscription } from 'rxjs';
import { textSpanIntersectsWith } from 'typescript';
import { WeatherRoot } from './models/weather';
import { WeatherService } from './services/weather.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'angularformation';

  public weather: WeatherRoot;
  private subs: Subscription[] = [];

  constructor(private weatherService: WeatherService) {
  }

  ngOnDestroy(): void {
    // map => iterate over an array
    this.subs.map(s => s.unsubscribe());
  }

  ngOnInit(): void {
   const sub:Subscription = this.weatherService.getWeather(-21.115141, 55.536384).subscribe(r => this.weather = r);
   this.subs.push(sub);
  }
}
