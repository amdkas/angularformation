import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerModule, NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { FormsModule } from '@angular/forms';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoService } from './services/todo.service';
import { ObservableDemoComponent } from './components/observable-demo/observable-demo.component';
import { WeatherService } from './services/weather.service';
import { HttpClientModule } from '@angular/common/http';
import { AddTodoComponent } from './components/add-todo/add-todo.component';
import { CreateUpdateTodoComponent } from './components/create-update-todo/create-update-todo.component';
import { UpdateTodoComponent } from './components/update-todo/update-todo.component';
import { NgbStringAdapter } from './services/ngbdate-adapter';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    ContactComponent,
    ColorPickerComponent,
    TodoListComponent,
    ObservableDemoComponent,
    AddTodoComponent,
    CreateUpdateTodoComponent,
    UpdateTodoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    MatSliderModule,
    FormsModule,
    HttpClientModule,
    NgbTooltipModule,
    NgbDatepickerModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr' },
    { provide: NgbDateAdapter, useClass: NgbStringAdapter },
    WeatherService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
