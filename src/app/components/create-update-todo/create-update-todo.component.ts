import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Todo } from 'src/app/models/todo';
import { NgbStringAdapter } from 'src/app/services/ngbdate-adapter';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-create-update-todo',
  templateUrl: './create-update-todo.component.html',
  styleUrls: ['./create-update-todo.component.scss']
})
export class CreateUpdateTodoComponent implements OnInit {

  @Input() public todo: Todo;

  constructor(private todoService: TodoService, private router: Router) { }

  ngOnInit(): void {
  }

  public saveTodo(): void {
    this.todoService.createOrUpdateTodo(this.todo).subscribe(() => this.router.navigate(['/']));
  }
}
