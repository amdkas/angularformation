import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-observable-demo',
  templateUrl: './observable-demo.component.html',
  styleUrls: ['./observable-demo.component.scss']
})
export class ObservableDemoComponent implements OnInit {

  public subject$: Subject<string>;
  public observable$: Observable<string>;

  constructor() { }

  ngOnInit(): void {
    this.subject$ = new Subject<string>();
    this.observable$ = this.subject$.asObservable();

    // Subscribe to the observable
    this.observable$.subscribe(v => console.info('New observable value received', v));
  }

  public emitValue(value: any): void {
    this.subject$.next(value);
  }

}
